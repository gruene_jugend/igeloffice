<?php

/**
 * Description of mailStandard_view
 *
 * @author KWM
 */
class mailStandard_view 
{
	public static $user;
	
	private static $POST_ATTRIBUT_MAIL_SUBMIT = 'submit';
	private static $POST_ATTRIBUT_MAIL_STANDARD_NONCE = 'io_mailStandard_nonce';
	private static $POST_ATTRIBUT_MAIL = 'io_mail';
	private static $POST_ATTRIBUT_MAIL_FORWARD = 'io_mailForward';
	private static $POST_ATTRIBUT_NOMAIL = "io_nomail";
	private static $MAIL_STANDARD_NONCE = 'io_mailStandard';
	
	public static function maskHandler() 
	{
		self::$user = new mailStandard_model(get_current_user_id());
		
		if(!self::$user->isMailPermitted && !self::$user->isMailForwardPermitted)
			return;
		
		wp_nonce_field(self::$MAIL_STANDARD_NONCE, self::$POST_ATTRIBUT_MAIL_STANDARD_NONCE);
		
		
		if(array_key_exists(mailStandard_view::$POST_ATTRIBUT_MAIL,$_POST)) //Der will ne Mailadresse
		{			
			if(self::$user->isMailPermitted) //Der darf ne Mailadresse
			{
				if(!isset($_POST[self::$POST_ATTRIBUT_MAIL]))
					return;
		
				$mailStandard_model = new mailStandard_model(get_current_user_id());
				mailStandard_control::delMailForward(get_current_user_id());
				mailStandard_control::setMail(get_current_user_id());
			}
		}		
		elseif(array_key_exists(mailStandard_view::$POST_ATTRIBUT_MAIL_FORWARD,$_POST)) //Der will ne Weiterleitung
		{
			if(self::$user->isMailForwardPermitted) //Der darf ne Weiterleitung
			{
				if(isset($_POST[self::$POST_ATTRIBUT_MAIL_SUBMIT]))
				{
					if(!isset($_POST[self::$POST_ATTRIBUT_MAIL_FORWARD]))
						return;
	
					$mailStandard_model = new mailStandard_model(get_current_user_id());
					mailStandard_control::delMail(get_current_user_id());
					mailStandard_control::setMailForward(get_current_user_id());
				}
			}
		}
		elseif(array_key_exists(mailStandard_view::$POST_ATTRIBUT_NOMAIL,$_POST)) //Der will nix
		{
			if(!isset($_POST[self::$POST_ATTRIBUT_NOMAIL]))
				return;
	
			mailStandard_control::delMail(get_current_user_id());
			mailStandard_control::delMailForward(get_current_user_id());

			/*$user =_wp_get_current_user();		
			$ldapConnector = ldapConnector::get(true);
			$ldapConnector->setUserAttribute($user->data->user_login,"mail", $user->data->user_email);*/
		}
		include_once(dirname(dirname(dirname(__FILE__))) . '/permission/mailStandard/templates/mail.php');
	}
}