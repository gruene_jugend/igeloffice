<form action="<?php echo($_SERVER["REQUEST_URI"]); ?>" method="post">

<input style="width:15px;margin-right:10px;transform:translate(0,2px);" class="chb" type="radio" name="io_nomail" id="io_nomail" value="true" <?php if(!self::$user->useMail && !self::$user->useMailForward) { echo ' checked'; } ?>><b>Keine Nutzung einer GJ-Mailadresse</b><br><br>
<input style="width:15px;margin-right:10px;transform:translate(0,2px);" class="chb" type="radio" name="io_mail" id="io_mail" value="true" <?php if(self::$user->useMail) { echo ' checked'; } ?>> <b>GJ-Postfach nutzen (<?php
    $x = self::$user->first_name . '.' . self::$user->last_name . "@gruene-jugend.de";
    $x = strtolower(io_umlaute(str_replace(' ', '',$x)));
    echo self::$user->mail;;
    ?>):</b> Mit einem Postfach kannst du Mails empfangen und gesondert von deinen privaten Postfächern verwalten. Außerdem ist es die einzige Möglichkeit, Mails mit deiner GJ-Mail-Adresse zu verfassen und zu verschicken. Falls dir die automatisch generierte Mailadresse zu lang ist oder anderweitig angepasst werden soll, <a href="mailto:webmaster@gruene-jugend.de">schreibe der*die Webmaster*in eine Mail.</a><br/><br/>
<input style="width:15px;margin-right:10px;transform:translate(0,2px);" class="chb" type="radio" name="io_mailForward" id="io_mailForward" value="true" <?php if(self::$user->useMailForward) { echo ' checked'; } ?>> <b>Weiterleitung an meine private Mail-Adresse:</b> Mit einer Weiterleitung werden alle Mails, die an <?php echo $x; ?> adressiert sind, an deine private Mail-Adresse weitergeleitet, die du im IGELoffice angegeben hast (<?php echo self::$user->user_email; ?>).<br/><br/>
<input type="submit" name="submit" id="submit" value="Übernehmen">
</form>
<hr/>

<h1>Hinweise</h2>
<h2>Mail-Abruf per Webclient</h2>
<p>Mittels des Webclients kannst du dein Postfach online im Browser verwalten, Mails lesen und schreiben.</p>
<p>Du findest den Webclient unter <b><a href="https://mail.gruene-jugend.de" target="_blank">mail.gruene-jugend.de</a></b>.</p>
Login-Name: <b><?php echo self::$user->mail; ?></b><br/>
Passwort: <b>Dein IGELoffice-Passwort</b><br/><br/>
<h2>Mail-Abruf mit Thunderbird, Outlook usw.</h2>
<p>Um dein Postfach in einem Mail-Client einzurichten, beachte bitte folgende Konfigurationen:</p>
<h3>Mail-Empfang (IMAP)</h3>
IMAP-Server: <b>mail.gruene-jugend.de</b><br/>
IMAP-Port: <b>143</b><br/>
Verbindungssicherheit: <b>STARTTLS</b><br/>
Benutzer*innenname: <b><?php echo self::$user->mail; ?></b><br/>
Passwort: <b>Dein IGELoffice-Passwort</b><br/><br/>
<h3>Mail-Empfang (SMTP)</h3>
Um Mails von deinem Mail-Client aus zu versenden, beachte bitte folgende Konfiguration:<br/><br/>

SMTP-Server: <b>mail.gruene-jugend.de</b><br/>
SMTP-Port: <b>587</b><br/>
Verbindungssicherheit: <b>STARTTLS</b><br/>
Benutzer*innenname: <b><?php echo self::$user->mail; ?></b><br/>
Passwort: <b>Dein IGELoffice-Passwort</b><br/><br/>
<h3>Bei Fehlermeldungen</h3>
Bei Problemen mit der Einrichtung deines Postfaches in einem Client, <b>vergewissere dich bitte, dass die oben genannte Konfiguration zu 100% so eingetragen sind, wie sie dort steht</b>. Die meisen Problemmeldungen mit Mail-Postfächern lassen sich mit Konfigurationsfehlern erklären.<br/><br/>

<script type="text/javascript">
    $(".chb").each(function() {
        $(this).change(function()
        {
            $(".chb").prop('checked',false);
            $(this).prop('checked',true);
        });
    });
</script>