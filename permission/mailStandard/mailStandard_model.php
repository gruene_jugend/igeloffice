<?php

/**
 * Description of mailPermission_model
 *
 * @author KWM
 */
class mailStandard_model extends User 
{
	public function __construct($id) 
	{
		writeLog("mailStandard_model.php: __construct()");
		parent::__construct($id);
	}
	
	public function __get($name) 
	{
		writeLog("mailStandard_model.php: __get()");
		if(parent::__get($name))
			return parent::__get($name);
				
		if($this == null)
			writeLog("mailStandard_model.php: this is null");

		$ldapConnector = ldapConnector::get();

		if($ldapConnector == null)
			writeLog("mailStandard_model.php: ldapConnector is null");

		if(!$ldapConnector->bind)
			{
				writeLog("mailStandard_model.php: ldapConnector is could not bind!");
				throw new Exception("mailStandard_model.php: ldapConnector could not bind! Maybe you are not logged in.");
			}

		writeLog("mailStandard_model.php: user_login = " . $this->user_login . " ID = " .$this->ID);
		writeLog("Current user: " . wp_get_current_user()->user_login);

		//Workaround
		$cur_user_login = wp_get_current_user()->user_login;
		$mail = $ldapConnector->getUserAttribute($cur_user_login, mailStandard_control::MAIL_ATTRIBUTE);
		$mailForward = $ldapConnector->getUserAttribute($cur_user_login, mailStandard_control::MAIL_FORWARD_ATTRIBUTE);

		//original
		//$mail = $ldapConnector->getUserAttribute($this->user_login, mailStandard_control::MAIL_ATTRIBUTE);
		//$mailForward = $ldapConnector->getUserAttribute($this->user_login, mailStandard_control::MAIL_FORWARD_ATTRIBUTE);
		
		if($mail == null)
			writeLog("mailStandard_model.php: mail is null");
		else
		{
			writeLog("mailStandard_model.php: mail is NOT null");
			if($mail["count"] > 1)
			{
				writeLog("mailStandard_model.php: More than one mail address");
				return new WP_Error("mailStandard_Error", "Es liegen mehrere Einträge für mail oder mail vor. Berechtigungssteuerung hier nicht richtig.");
			}
		}

		if($mailForward == null)
			writeLog("mailStandard_model.php: mailForward is null");
		else
		{
			writeLog("mailStandard_model.php: mailForward is NOT null");

			if($mailForward["count"] > 1)
			{
				writeLog("mailStandard_model.php: More than one forwarding mail address");
				return new WP_Error("mailStandard_Error", "Es liegen mehrere Einträge für mail oder mailForwardAddress vor. Berechtigungssteuerung hier nicht richtig.");
			}
		}

		unset($mail['count']);
		unset($mailForward['count']);

		if($name == "isMailPermitted")
		{
			$x = mailStandard_control::getMailPermission();

			if($x != null)
			{
				writeLog("mailStandard_model.php: Got MailPermission");
				return User_Control::isPermitted($this->ID, $x->id);
			}
			else
			{
				writeLog("mailStandard_model.php: Couldn't get MailPermission");
				return false;
			}
		}
		else if($name == "isMailForwardPermitted")
		{
			$x = mailStandard_control::getMailForwardPermission();

			if($x != null)
			{
				writeLog("mailStandard_model.php: Got MailForwardingPermission");
				return User_Control::isPermitted($this->ID, $x->id);
			}
			else
			{
				writeLog("mailStandard_model.php: Couldn't get MailForwardingPermission");
				return false;
			}
		}
		else if($name == "useMail") 
		{		
			$mailAttribute = $ldapConnector->getUserAttribute($this->user_login, mailStandard_control::MAIL_ATTRIBUTE);
			
			if(str_replace("@gruene-jugend.de", "", $mailAttribute[0]) != $mailAttribute[0])
				return true;

			return false;
		} 
		else if($name == "useMailForward") 
		{
			$mailForwardAttribute = $ldapConnector->getUserAttribute($this->user_login, mailStandard_control::MAIL_FORWARD_ATTRIBUTE);
			
			if($mailForwardAttribute != "" && str_replace("@gruene-jugend.de", "", $mailForwardAttribute[0]) != $mailForwardAttribute[0])
				return true;
			
			return false;
		}
		else if($name == "mail") 
		{
			if($this->useMail)
				return $ldapConnector->getUserAttribute($this->user_login, mailStandard_control::MAIL_ATTRIBUTE)[0];
			
			if($this->useMailForward)
				return $ldapConnector->getUserAttribute($this->user_login, mailStandard_control::MAIL_FORWARD_ATTRIBUTE)[0];
			
			if($this->art == "user")
				return str_replace(' ', '', strtolower(io_umlaute($this->first_name)) . "." . strtolower(io_umlaute($this->last_name)) . "@gruene-jugend.de");
			
			return str_replace(' ', '', strtolower(io_umlaute($this->user_login)) . "@gruene-jugend.de");
		} 
	}
}