<?php

/**
 * Description of mailStandard_Control
 *
 * @author KWM
 */
class cloud_view {
    const POST_SUBMIT = "io_cloud_submit";
    const POST_NONCE = "io_cloud_nonce";

    const NONCE = "nonce";

    public static function maskHandler() 
    {
		$owncloud_model = new cloud_model(get_current_user_id());

        if(!$owncloud_model->isPermitted)
            return;
        
        include(dirname(dirname(dirname(__FILE__))) . '/permission/cloud/templates/cloud.php');

        if($owncloud_model->isSpacePermitted)
        {
            if(isset($_POST[self::POST_SUBMIT]))
            {
                if( !isset($_POST[self::POST_NONCE]) || !wp_verify_nonce($_POST[self::POST_NONCE], self::NONCE))
                    return;
                
                Request_Control::create(get_current_user_id(), Request_Cloud::art());
                echo "<h1>Cloudspeicher beantragt.</h1>";
                echo '<script>alert("Cloudspeicher beantragt!");</script>';
            }            
        }
	}
}