Die GJ-Cloud ist die zentrale Datenverwaltung der GRÜNEN JUGEND. Sie ist dazu da, Dateien von GJ-Strukturen zentral zu verwalten und zu speichern. Sie dient außerdem dem zentralen Wissensmanagement der GRÜNEN JUGEND und unterstützt dabei die ehrenamtliche Arbeit.
<hr>

<h1>Nutzung</h1>
Als Mitglied ohne ein Amt beim Bundesverband steht dir in der Regel in der Cloud nur die Materialdatenbank zur Verfügung. Dort sind Logos, Vorlagen und anderes abgespeichert.<br><br>

Solltest du ein Amt beim Bundesverband innehaben, so steht dir dort der Ordner des jeweiligen Gremiums zur Verfügung, bspw. der Ordner eines Fachforums.<br><br>
Grundsätzlich sollte sich der Sinn und Zweck der Stammordner anhand seines Namens ableiten lassen. Beachte bitte: Die Cloud ist nicht dein persönlicher Cloudspeicher. Du hast als Person nicht die Möglichkeit, Daten in deinem persönlichen Space hochzuladen oder sie dorthin zu verschieben.
<hr>
<h2>Zugangsdaten</h2>
Cloud-URL: <b><a href="https://cloud.gruene-jugend.de" target="_blank">cloud.gruene-jugend.de</a></b><br>
Benutzer*innenname: <b><?php echo $owncloud_model->user_login; ?></b><br>
Passwort: <b>Dein IGELoffice-Passwort</b>
<hr>

<?php
/**
 * Created by PhpStorm.
 * User: KWM
 * Date: 30.12.2016
 * Time: 15:27
 */
?>

<h1>Als GJ-Gliederung Cloudspeicher beantragen</h1>

Cloud-Space kann für die eigene Arbeit sehr wichtig sein. Die Cloud bildet dabei die gemeinsame Arbeitsplattform für die Gruppe. Es ermöglicht die Ausarbeitung von Projekten, sowie das Teilen von Dateien an andere, außenstehende.<br><br>
Landesverbände und Basisgruppen können für einen kleinen monatlich Betrag unbegrenzten Cloudspeicher erhalten (2€ pro Monat pro Basisgruppe und 5€ pro Monat pro Landesverband). <br><br>

Folgende Landesverbände haben Cloud-Speicher gemietet (Stand: 01.12.2018): 
<ul>
    <li>Berlin</li> 
    <li>Bremen</li> 
    <li>Hessen</li> 
    <li>Sachsen</li>
    <li>Thüringen</li>
</ul>
Solltest du einer Basisgruppe in einem solchen Landesverband angehören und Cloud-Speicher für deine Basisgruppe benötigen, solltest du dich an deinen Landesvorstand oder die Landesgeschäftsstelle wenden, um kein Geld ausgeben zu müssen.<br><br>
<?php

    if(!$owncloud_model->hasSpace) 
    {
?>
        <form action="<?php echo($_SERVER["REQUEST_URI"]); ?>" method="post">
            <?php
                wp_nonce_field(self::NONCE, self::POST_NONCE);
            ?>
            <input type="submit" name="<?php echo cloud_view::POST_SUBMIT; ?>" value="Cloudspeicher zahlungspflichtig beantragen">
        </form><br><br>

        Mit Betätigung des Buttons beantragst du Cloudspeicher, der von der Webmasterei anschließend eingerichtet werden muss. Das kann unter Umständen ein paar Tage dauern. Eine Rechnung wird euch (halb-)jährlich postalisch zugestellt.
<?php
    } else 
    {
?>
        Du besitzt einen Cloud-Ordner, denn du beim Login in die Cloud sehen und nutzen kannst. Um anderen den Zugriff auf deinen Cloud-Ordner zu ermöglichen, gehe zur <a href="wp-admin/post.php?post=<?php

        echo get_page_by_title("Cloud " . (new User(get_current_user_id()))->user_login)->ID;

        ?>&action=edit">Gruppenverwaltung</a> und füge die entsprechenden Nutzer*innen der Gruppe hinzu.<br><br>
        Beachte bitte: Die neuen Nutzer*innen müssen bereits im IGELoffice registriert sein.
<?php
    }

?>