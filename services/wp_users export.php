<?php 

/*
* Exportiert user_login, user_email und display_name aus der Tabelle wp_users ins Stammverzeichnis des Plugins
* Zum Ausführen einfach     require_once '/services/wp_users export.php'    an IGELoffice.php anfügren
*/

$servername = "localhost";
$username = DB_USER;
$password = DB_PASSWORD;
$dbname = DB_NAME;
$max_users = -1;

$mysql = new mysqli($servername, $username, $password, $dbname);
if ($mysql->connect_error)
    throw new mysqli_sql_exception("SQL-Connection failed: " . $mysql->connect_error);

$csv_file = fopen(dirname(dirname(__FILE__)) . "/wp_users.csv", "w", false);
$csv_line = array("user_login", "user_email", "display_name", date('d. M Y, H:i:s'));
fputcsv($csv_file,$csv_line);


$sql = "SELECT user_login,user_email,display_name FROM wp_users";
$result = $mysql->query($sql);

if($result->num_rows > 0)
{    
    while($row = $result->fetch_assoc())
    {
        $csv_line = array($row["user_login"], $row["user_email"], $$row["display_name"]);
        fputcsv($csv_file, $csv_line);
    }
}

$result->free();
fclose($csv_file);
$mysql->close();

?>