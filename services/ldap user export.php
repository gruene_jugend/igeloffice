<?php

/*
* Exportiert CN, mail, mailAlternateAddress und mailForwardingAddress (alle) aus dem LDAP ins Stammverzeichnis des Plugins
* Zum Ausführen einfach     add_action("wp_loaded", function() {require_once 'services/ldap users export.php';});    an IGELoffice.php anfügren
*/

$filter = "(cn=*)";
$attributes = array("cn", "mail", "mailAlternateAddress", "mailForwardingAddress");


$ldap_connection = ldap_connect(LDAP_HOST, LDAP_PORT);
if($ldap_connection)
{
    ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3);
    $ldap_bind = ldap_bind($ldap_connection,LDAP_PROXY_USER, LDAP_PROXY_PW);
    
    if($ldap_bind)
    {
        $result = ldap_search($ldap_connection, LDAP_USER_BASE, $filter, $attributes);
        $info = ldap_get_entries($ldap_connection, $result);

        $csv_file = fopen(dirname(dirname(__FILE__)) . "/ldap_users.csv", "w", false);
        fputcsv($csv_file, $attributes);        

        for($i = 0; $i < $info["count"]; $i++)
        {
            $cn = $info[$i]["dn"];
            $mail = null;
            $forward = null;
            $alternate = null;            

            if(array_key_exists("mail", $info[$i]))
                $mail = $info[$i]["mail"][0];
            else
                $mail = null;

            if(array_key_exists("mailalternateaddress", $info[$i]))
                $alternate = $info[$i]["mailalternateaddress"][0];
            else
                $alternate = null;

            if(array_key_exists("mailforwardingaddress", $info[$i]))
                {
                    if($info[$i]["mailforwardingaddress"]["count"] > 1)
                        {
                            foreach($info[$i]["mailforwardingaddress"] as $address)
                                $forward .= $address . ";\n";
                        }
                    else
                        $forward = $info[$i]["mailforwardingaddress"][0];
                }
            else
                $forward = null;

            fputcsv($csv_file, array($cn, $mail, $alternate, $forward));
        }
            

        fclose($csv_file);
        ldap_free_result($result);
        ldap_unbind($ldap_connection);
    }
}

?>