<?php
/**
 * TODO: FRAGESTELLUNG, WAS PASSIERT BEI SET? WAS, WENN ATTRIBUTE ENTFERNT WERDEN? WAS, WENN HINZUGEFÜGT? WAS, WENN NUR GEÄNDERT?
 * TODO: Permissions werden nicht in LDAP erstellt
 */


/**
 * Description of ldapConnector
 *
 * @author Andreas Krischer, akbyte
 */
class ldapConnector extends LDAP 
{
	static function writeLogLDAP($message)
	{
    	openlog("IGELoffice", 0, 0);
    	syslog(LOG_INFO, $message);
    	closelog();
	}

	/**
	 * instance of this class
	 * @var ldapConnector
	 */
	private static $instance;

	/**
	 * connects to LDAP
	 */
	protected function __construct()
	{
		self::writeLogLDAP("ldapConnector.php: __construct()");
		//These constants should be definied in wp-config.php
		parent::__construct(LDAP_HOST, LDAP_PORT);
	}

	/**
	 * get only instance of this class
	 * @param  boolean $bind if the class should bind to the LDAP server after connecting
	 * @return ldapConnector        instance of this class
	 */
	public static function get($bind = true) 
	{
		self::writeLogLDAP("ldapConnector.php: get()");

		if(self::$instance instanceof ldapConnector)		
			return self::$instance;

		self::$instance = new ldapConnector();

		if($bind && !self::$instance->bind)
			if(!self::$instance->bind())
				throw new Exception("Could not bind to LDAP Server. Invalid credentials?");

		return self::$instance;
	}

	/**
	 * binds to ldap server
	 * @param  string $user username. if username and password are read from database & cookies
	 * @param  string $pass password - see username
	 * @return boolean       successfull or not
	 */
	public function bind($user = null, $pass = null) 
	{
		self::writeLogLDAP("ldapConnector.php: IO_bind()");
		
		if(is_null($user) && is_null($pass))
		{
			//get ldap-password from db & cookie
			if(!is_user_logged_in())
				return false;

			$user_id = get_current_user_id();
			$userinfo = get_userdata($user_id);
			$user = $userinfo->user_login;
			$username_hash = hash('sha256', $user);
			$pass_hash = base64_decode(get_user_meta($user_id, '_ldap_pass', true));

			if(array_key_exists($username_hash,$_COOKIE))
				$key = base64_decode($_COOKIE[$username_hash]);
			else //if user_login was not username but email
				{
					$user = $userinfo->user_email;
					$username_hash = hash('sha256', $user);
					$pass_hash = base64_decode(get_user_meta($user_id, '_ldap_pass', true));
					
					if(array_key_exists($username_hash,$_COOKIE))
						$key = base64_decode($_COOKIE[$username_hash]);
					else
						$key = null;
				}

			if(empty($pass_hash) || empty($key))
				return false;

			require_once IGELOFFICE_PATH . '/control/php-encryption/Crypto.php';
			$pass = Crypto::decrypt($pass_hash, $key);
		}
		return parent::bind($this->userDN($user), $pass);
	}

	private function userDN($user)
	{
		self::writeLogLDAP("ldapConnector.php: userDN()");
		return 'cn=' . $user . ',' . LDAP_USER_BASE;
	}
	

	/**
	 * this is called before every public LDAP query method.
	 * @param  string $method method name
	 * @param  array $args   arguments for method
	 * @return mixed         return of method
	 */
	public function __call($method, $args) 
	{
		self::writeLogLDAP("ldapConnector.php: $method()");
		if(method_exists($this, $method)) 
		{
			if(!$this->isBound())
				return false;
			
			return call_user_func_array(array($this, $method), $args);
		}
	}

	/**
	 * adds a LDAP user
	 * @param string $firstname firstname of user
	 * @param string $surname   lastname of user
	 * @param string $mail      mail address of user
	 * @return boolean successful or not
	 * @todo check if DN already exists
	 * @todo password sending
	 */
	private function addUser($firstname, $surname, $mail) 
	{ 
		self::writeLogLDAP("ldapConnector.php: addUser()");
		//check if user or DN exists!
		if(empty($firstname) || empty($surname) || empty($mail)) 		
			return new WP_Error('ldap_add_user_nodata', 'Der User benötigt einen Vornamen, Nachnamen und eine gültige E-Mail-Adresse.');
		
		if(!ldap_add($this->res, $this->userDN($firstname.' '.$surname), array(
			'cn' => $firstname.' '.$surname,
			'sn' => $surname,
			'mail' => $mail,
			'mailAlternateAddress' => $mail,
			'objectClass' => array(
				'top',
				'person',
				'inetOrgPerson',
				'qmailUser'
			)))) 
			{
				self::writeLogLDAP("addUser: " . ldap_error($this->res));
				return $this->error();
			}
		return true;
	}

	private function addOrgaUser($firstname, $mail) 
	{ 
		self::writeLogLDAP("ldapConnector.php: addOrgaUser()");

		//check if user or DN exists!
		if(empty($firstname) || empty($mail))
			return new WP_Error('ldap_add_user_nodata', 'Der User benötigt einen Vornamen, Nachnamen und eine gültige E-Mail-Adresse.');
		
		if(!ldap_add($this->res, $this->userDN($firstname), array(
			'cn' => $firstname,
			'sn' => $firstname,
			'mail' => $mail,
			'mailAlternateAddress' => $mail,
			'objectClass' => array(
				'top',
				'person',
				'inetOrgPerson',
				'qmailUser'
			)
		))) {
			self::writeLogLDAP("ldapConnector.php: addOrgaUser: " . ldap_error($this->res));
			return $this->error();
		}
		return true;
	}

	/**
	 * gives a user a LDAP permission
	 * @param string $user       user CN
	 * @param string $permission permission CN
	 * @return boolean successful or not
	 */
	private function addUserPermission($user, $permission) 
	{
		self::writeLogLDAP("ldapConnector.php: addUserPermission()");
		if(!ldap_mod_add($this->res, $this->permissionDN($permission), array('member' => $this->userDN($user)))) 
		{
			self::writeLogLDAP("ldapConnector.php: addUserPermission: " . ldap_error($this->res));
			return $this->error();
		}
		return true;
	}

	public function permissionDN($permission)
	{
		self::writeLogLDAP("ldapConnector.php: permissionDN()");
		return 'cn=' . $permission . ',' . LDAP_PERMISSION_BASE;
	}

	/**
	 * replaces all group members with the provided array of CN's
	 * @param array $users user CN's
	 * @param string $group group CN
	 * @return boolean successful or not
	 */
	public function addUsersToGroup($users, $group)
	{
		self::writeLogLDAP("ldapConnector.php: addUsersToGroup()");
		array_walk($users, function(&$user) 
		{
			$user = 'cn='.$user.','.LDAP_USER_BASE;
		});
		
		if(!ldap_mod_add($this->res, $this->groupDN($group), array('member' => $users))) 
		{
			self::writeLogLDAP("ldapConnector.php: addUsersToGroup: " . ldap_error($this->res));
			return $this->error();
		}

		return true;
	}

	public function groupDN($group)
	{
		self::writeLogLDAP("ldapConnector.php: groupDN()");
		return 'cn=' . $group . ',' . LDAP_GROUP_BASE;
	}

	/**
	 * set password for LDAP user
	 * @param string $user     user CN
	 * @param string $password password
	 * @return boolean successful or not
	 */
	public function setUserPassword($user, $password) 
	{
		self::writeLogLDAP("ldapConnector.php: setUserPassword()");
		if(!ldap_mod_replace($this->res, $this->userDN($user), array(
			'userPassword' => "{SHA}" . base64_encode(pack( "H*", sha1($password))),
			'qmailGID' => intval(time() / 86400) //last password change - days since 01.01.1970
		))) {
			self::writeLogLDAP("ldapConnector.php: setUserPassword: " . ldap_error($this->res));
			return $this->error();
		}
		return true;
	}

	/**
	 * gets an user attribute
	 * @param  string $user      user CN
	 * @param  string $attribute attribute name
	 * @return array            values for this attribute and "count" of values
	 */
	public function getUserAttribute($user, $attribute) 
	{
		self::writeLogLDAP("ldapConnector.php: getUserAttribute()");
		return $this->getAttribute($this->userDN($user), $attribute);
	}

	/**
	 * checks if a user exists in LDAP
	 * @param  string  $user user CN
	 * @return boolean       yes or no
	 */
	public function isLDAPUser($user) 
	{
		self::writeLogLDAP("ldapConnector.php: isLDAPuser()");
		return $this->DNexists($this->userDN($user));
	}

	/**
	 * deletes LDAP user
	 * @param  string $user user CN
	 * @return boolean       successful or not
	 */
	public function delUser($user) 
	{
		self::writeLogLDAP("ldapConnector.php: delUser()");
		if(!ldap_delete($this->res, $this->userDN($user))) 
		{
			self::writeLogLDAP("ldapConnector.php: delUser: " . ldap_error($this->res));
			return $this->error();
		}
		return true;
	}

	/**
	 * removes user from LDAP permission
	 * @param  string $user       user CN
	 * @param  string $permission permission CN
	 * @return boolean             successful or not
	 */
	public function delUserPermission($user, $permission)
	{
		self::writeLogLDAP("ldapConnector.php: delUserPermission()");

		if(!ldap_mod_del($this->res, $this->permissionDN($permission), array('member' => $this->userDN($user)))) 
		{
			self::writeLogLDAP("ldapConnector.php: delUserPermission: " . ldap_error($this->res));
			return $this->error();
		}
		return true;
	}

	/**
	 * removes user from LDAP group
	 * @param  string $user  user CN
	 * @param  string $group group CN
	 * @return boolean        successful or not
	 */
	public function delUserFromGroup($user, $group) 
	{
		self::writeLogLDAP("ldapConnector.php: delUserFromGroup()");

		if(!ldap_mod_del($this->res, $this->groupDN($group), array('member' => $this->userDN($user)))) 
		{
			self::writeLogLDAP("ldapConnector.php: delUserFromGroup: " . ldap_error($this->res));
			return $this->error();
		}
		return true;
	}
	
	/**
	 * adds LDAP group
	 * @param string $group group CN
	 * @return boolean successful or not
	 */
	public function addGroup($group) 
	{
		self::writeLogLDAP("ldapConnector.php: addGroup()");

		if(!ldap_add($this->res, $this->groupDN($group), array(
			'cn' => $group,
			'member' => '',
			'objectClass' => array(
				'groupOfNames',
				'gjGroup',
				'top'
			)
		))) {
			self::writeLogLDAP("ldapConnector.php: addGroup: " . ldap_error($this->res));
			return $this->error();
		}
		return true;
	}

	/**
	 * adds an group to and group
	 * @param string $groupToAdd group CN to add as member
	 * @param string $group      group CN
	 */
	public function addGroupToGroup($groupToAdd, $group) 
	{
		self::writeLogLDAP("ldapConnector.php: addGroupToGroup()");
		return $this->setAttribute($this->groupDN($group), 'member', $this->groupDN($groupToAdd));
	}

	/**
	 * gives a group a permission in LDAP
	 * @param string $group      group CN
	 * @param string $permission permission CN
	 * @return boolean successful or not
	 */
	public function addGroupPermission($group, $permission) 
	{
		self::writeLogLDAP("ldapConnector.php: addGroupPermission()");
		if(!ldap_mod_add($this->res, $this->permissionDN($permission), array(
			'member' => $this->groupDN($group)))) 
			{
				self::writeLogLDAP("ldapConnector.php: addGroupPermission: " . ldap_error($this->res));
				return $this->error();
			}
		return true;
	}

	/**
	 * gets an attribute of a group
	 * @param  string $group     group CN
	 * @param  string $attribute attribute name
	 * @return array            values for attribute and value "count"
	 */
	public function getGroupAttribute($group, $attribute) 
	{
		self::writeLogLDAP("ldapConnector.php: getGroupAttribute()");
		return $this->getAttribute($this->groupDN($group), $attribute);
	}
	
	/**
	 * returns a list of users in a group
	 * @param  string $group group name
	 * @return array        list of CNs
	 */
	public function getAllGroupMembers($group) 
	{
		self::writeLogLDAP("ldapConnector.php: getAllGroupMembers()");
		return $this->getCNList($this->groupDN($group), 'member', 'users');
	}

	/**
	 * returns a list of groups in a group
	 * @param  string $group group name
	 * @return array        list of CNs
	 */
	public function getAllGroupGroups($group) 
	{
		self::writeLogLDAP("ldapConnector.php: getAllGroupGroups()");
		return $this->getCNList($this->groupDN($group), 'member', 'groups');
	}

	/**
	 * returns a list of owners of a group
	 * @param  string $group group name
	 * @return array        list of CNs
	 */
	public function getAllGroupLeaders($group) 
	{
		self::writeLogLDAP("ldapConnector.php: getAllGroupLeaders()");
		return $this->getCNList($this->groupDN($group), 'owner');
	}

	public function getGroupsOfLeader($leader) 
	{
		self::writeLogLDAP("ldapConnector.php: getGroupsOfLeader()");

		$search = $this->search(LDAP_GROUP_BASE, "(owner=" . $this->userDN($leader) . ")", array('cn'));
		if($search) {
			$array = array();
			foreach($search AS $result) {
				$array[] = $result['cn'][0];
			}
			return $array ;
		}
		return false;
	}

	public function getGroupPermissions($group) 
	{
		self::writeLogLDAP("ldapConnector.php: getGroupPermissions()");
		return $this->getMemberOfList($this->groupDN($group), 'permissions');
	}

	/**
	 * deletes LDAP group
	 * @param  string $group group CN
	 * @return boolean        successful or not
	 */
	public function delGroup($group) 
	{
		self::writeLogLDAP("ldapConnector.php: delGroup()");

		if(!ldap_delete($this->res, $this->groupDN($group))) 
		{
			self::writeLogLDAP("ldapConnector.php: delGroup: " . ldap_error($this->res));
			return $this->error();
		}
		return true;
	}

	//TODO: VORSICHT - WAS BEI MEHRERE ATTRIBUTE? RÜCKGABE ALS ARRAY?

	/**
	 * removes a LDAP permission from a group
	 * @param  string $group      group CN
	 * @param  string $permission permission CN
	 * @return boolean             successful or not
	 */
	public function delGroupPermission($group, $permission) 
	{
		self::writeLogLDAP("ldapConnector.php: delGroupPermission()");

		if(!ldap_mod_del($this->res, $this->permissionDN($permission), array('member' => $this->groupDN($group)))) 
		{
			self::writeLogLDAP("ldapConnector.php: delGroupPermission: " . ldap_error($this->res));
			return $this->error();
		}
		return true;
	}

	/**
	 * deletes group from group
	 * @param  string $groupToDel child group to delete
	 * @param  string $group      group CN of parent group
	 * @return boolean             successful or not
	 */
	public function delGroupFromGroup($groupToDel, $group) 
	{
		self::writeLogLDAP("ldapConnector.php: delGroupFromGroup()");
		return $this->delAttribute($this->groupDN($group), array('member' => $this->groupDN($groupToDel)));
	}

	/**
	 * adds a LDAP-permission
	 * @param string $permission permission CN
	 * @return boolean successful or not
	 */
	public function addPermission($permission) 
	{
		self::writeLogLDAP("ldapConnector.php: addPermission()");

		if(!ldap_add($this->res, $this->permissionDN($permission), array(
			'cn' => $permission,
			'objectClass' => 'groupOfNames',
			'member' => ''
		))) {
			self::writeLogLDAP("ldapConnector.php: addPermission: " . ldap_error($this->res));
			return $this->error();
		}
		return true;
	}

	/**
	 * deletes permission
	 * @param  string $permission permission CN
	 * @return boolean             successful or not
	 */
	public function delPermission($permission) 
	{
		self::writeLogLDAP("ldapConnector.php: delPermission()");

		if(!ldap_delete($this->res, $this->permissionDN($permission))) 
		{
			self::writeLogLDAP("ldapConnector.php: delPermission: " . ldap_error($this->res));
			return $this->error();
		}
		return true;
	}

	public function getUserPermissions($user) 
	{
		self::writeLogLDAP("ldapConnector.php: getUserPermissions()");
		return $this->getMemberOfList($this->userDN($user), 'permissions');
	}

	//TODO: Sowohl Berechtigungen durch Permission und Group berücksichtigen

	public function getAllUserPermissions($user) 
	{
		self::writeLogLDAP("ldapConnector.php: getAllUserPermissions()");

		$search = $this->search(LDAP_PERMISSION_BASE, null, array('cn'));
		if($search)
			return $this->DNtoCN($search, 'permissions');
		
		return false;

	}
	
	public function getPermissionAttribute($permission, $attribute) 
	{
		self::writeLogLDAP("ldapConnector.php: getPermissionAttribute()");
		return $this->getAttribute($this->permissionDN($permission), $attribute);
	}
	
	public function getPermissionedUser($permission) 
	{
		self::writeLogLDAP("ldapConnector.php: getPermissionedUser()");
		return $this->getCNList($this->permissionDN($permission), 'member', 'users');
	}

	public function getUserGroups($user) 
	{
		self::writeLogLDAP("ldapConnector.php: getUserGroups()");
		return $this->getMemberOfList($this->userDN($user), 'groups');
	}

	//TODO: Array bei Value

	public function isQualified($user, $permission) 
	{
		self::writeLogLDAP("ldapConnector.php: isQualified()");

		if($this->searchCN(LDAP_PERMISSION_BASE, $permission))		
			return true;		
		return false;
	}
	
	/**
	 * Deletes an attribute from a group
	 * @param  string $group     Group name
	 * @param  string $attribute attribute
	 * @param  string $value     value to delete
	 * @return bool            successfull or not
	 */
	public function delGroupAttribute($group, $attribute, $value) 
	{
		self::writeLogLDAP("ldapConnector.php: delGroupAttribute()");
		return $this->delAttribute($this->groupDN($group), array($attribute => $value));
	}

	//TODO: Was heißt hier $mode = 'add'? Manchmal muss ein Attribute als komplett neues Attribut hinzugefügt werden, manchmal geändert werden

	/**
	 * Deletes an attribute from a permission
	 * @param  string $group     permission name
	 * @param  string $attribute attribute
	 * @param  string $value     value to delete
	 * @return bool            successfull or not
	 */
	public function delPermissionAttribute($permission, $attribute, $value) 
	{
		self::writeLogLDAP("ldapConnector.php: delPermissionAttribute()");
		return $this->delAttribute($this->permissionDN($permission), array($attribute => $value));
	}

	//TODO

	/**
	 * Deletes an attribute from an user
	 * @param  string $group     user name
	 * @param  string $attribute attribute
	 * @param  string $value     value to delete
	 * @return bool            successfull or not
	 */
	public function delUserAttribute($user, $attribute, $value) 
	{
		self::writeLogLDAP("ldapConnector.php: delUserAttribute()");
		return $this->delAttribute($this->userDN($user), array($attribute => $value));
	}



	public function setGroupAttribute($group, $attribute, $value, $mode = 'add', $old_value = null) 
	{
		self::writeLogLDAP("ldapConnector.php: setGroupAttribute()");
		$this->setAttribute($this->groupDN($group), $attribute, $value, $mode, $old_value);
	}

	public function setPermissionAttribute($permission, $attribute, $value, $mode = 'add', $old_value = null) 
	{
		self::writeLogLDAP("ldapConnector.php: setPermissionAttribute()");
		$this->setAttribute($this->permissionDN($permission), $attribute, $value, $mode, $old_value);
	}

	public function setUserAttribute($user, $attribute, $value, $mode = 'add', $old_value = null) 
	{
		self::writeLogLDAP("ldapConnector.php: setUserAttribute()");
		$this->setAttribute($this->userDN($user), $attribute, $value, $mode, $old_value);
	}	

	public function isServerDomain($domain) 
	{
		self::writeLogLDAP("ldapConnector.php: isServerDomain()");
		return $this->DNexists($this->domainDN($domain));
	}

	public function domainDN($domain) 
	{
		self::writeLogLDAP("ldapConnector.php: domainDN()");
		return 'cn='.$domain.','.LDAP_DOMAIN_BASE;
	}

}