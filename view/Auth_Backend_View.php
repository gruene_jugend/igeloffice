<?php

/**
 * Description of Auth_Backend_View
 *
 * @author KWM
 */
class Auth_Backend_View 
{
	public static function authentifizierung($user, $user_name, $password) 
	{
		if(empty($user_name) || empty($password))
			return;
		
		if(!($user instanceof WP_User)) 
		{
			$user = get_user_by('login', $user_name);
			
			if($user == null)
				$user = get_user_by("email",$user_name);

			//Falls User nicht in der Datenbank des IO ist
			if(!($user instanceof WP_User))
				return new WP_Error('user_not_exists', 'Du bist anscheinend noch nicht im IGELoffice registriert.');
		}
		

		
		$user_id = $user->ID;
		$active = get_user_meta($user_id, User_Util::ATTRIBUT_AKTIV, true);

		//Falls User nicht als aktiv markiert wurde
		if($active != "1")
			return new WP_Error("user_inaktiv", "<strong>FEHLER:</strong> Dein Account wurde noch nicht aktiviert!");
		
		try
		{
			$ldapConn = ldapConnector::get(false);
			
			if($ldapConn->bind($user_name, $password)) 
			{
				DebugConsole("Login");
				//save password hash in database and key in cookie

				//for the new php-encryption version
				//include(dirname(dirname(__FILE__)) . '/control/php-encryption/src/Crypto.php');

				//old php-encryption version
				include(dirname(dirname(__FILE__)) . '/control/php-encryption/Crypto.php');
				
				$key = Crypto::createNewRandomKey();
				$hash = base64_encode(Crypto::encrypt($password, $key));
				setcookie(hash('sha256', $user_name), base64_encode($key));
				unset($key, $password);
				update_user_meta($user_id, '_ldap_pass', $hash);
				wp_set_current_user($user_id, $user_name);
				writeLog("Auth_Backend_View.php: Current user set (ID: " . $user_id . ", login: " . $user_name . ")");

				DebugConsole("Auth_Backend_View.php: LDAP-PW cached");
			}
			elseif($user_name != null)
				{
					DebugConsole("Auth_Backend_View.php: LDAP-PW caching failed. LDAP connection broken?");
					return new WP_Error('ldap_login_failed', 'Deine Zugangsdaten sind nicht korrekt.');
				}
		} 
		catch(Exception $ex) 
		{
			DebugConsole("Auth_Backend_View.php: LDAP-PW caching failed");
			return new WP_Error('ldap_login_failed', 'Deine Zugangsdaten sind nicht korrekt.');
		}

		return $user;
	}
}